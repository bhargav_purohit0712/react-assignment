import css from './error.module.css'

const Error = ({ err, sus, errMsg, susMsg }) => {
    return err ? (
    <h1 className={css.active}>
      {' '}
      <p className={css.errMsg}> {errMsg}</p>{' '}
    </h1>
  ) : sus ? (
    <h1 className={css.active}>
      {' '}
      <p className={css.susMsg}>{susMsg}</p>{' '}
    </h1>
  ) : null
}

export default Error
