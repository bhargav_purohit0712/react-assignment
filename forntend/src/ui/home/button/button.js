import React from 'react';
import css from './button.module.css'

const Button = ({method,text}) =>{
     
     return <button type='button' className={css.button} onClick={() => method()}> {text} </button>
    
}
export default Button