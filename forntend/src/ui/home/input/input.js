import React from 'react'
import css from './input.module.css'

const Input = ({ type, method, placeholder, valued }) => {
  return (
    <input
      type={type}
      value={valued}
      onChange={e => method(e)}
      className={css.input}
      placeholder={placeholder}
    />
  )
}

export default Input
