import css from './button.module.css'

const Button = ({ type, name }) => {
  return (
    <div className={css.btn}>
      <button type={type}>{name}</button>
    </div>
  )
}
export default Button
