import React from 'react'
import css from './ip.module.css'
const Input = ({ labelName, type, name, id, value, onInput,len }) => {
  return (
    <div className={css.formEle}>
      <label htmlFor={labelName}>{labelName}</label>
      <br />
      <input
        type={type}
        name={name}
        id={id}
        minLength={len}
        required
        value={value}
        onChange={onInput}
        autoComplete='on'
      />
    </div>
  )
}

export default Input
