import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import 'bootstrap/dist/css/bootstrap.min.css'
import { BrowserRouter as Router } from 'react-router-dom'

import { CookiesProvider } from 'react-cookie'

ReactDOM.render(
  <Router initialEntries={['/']}>
      <App />
  </Router>,
  document.getElementById('root')
)

