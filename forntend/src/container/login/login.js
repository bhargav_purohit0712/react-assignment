import React, { useState } from 'react'

import css from './login.module.css'
import { NavLink } from 'react-router-dom'

import { useHistory } from 'react-router-dom'
import cookie from 'js-cookie'

import Input from '../../ui/input/input'
import Error from '../../ui/error/error'
import Button from '../../ui/button/button'

//import axios from 'axios'
const Login = props => {
  const [email, setEmail] = useState('')
  const [pass, setPass] = useState('')
  const [err, setErr] = useState(false)
  const [errMsg, setErrMsg] = useState('')

  const history = useHistory()

  const login = e => {
    e.preventDefault()
    const data = {
      email: email,
      pass: pass
    }
    fetch('http://localhost:8080/api/login', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(res => res.json())
      .then(data => {
        if (data.err) {
          setErr(true)
          setErrMsg(data.err)
        } else {
          cookie.set('token', data.token, { expires: 1 / 24 })
          cookie.set('uid', data.userId, { expires: 1 / 24 })
          if (data.token && data.userId) {
            history.replace('/home')
          } else {
            setErr(true)
            setErrMsg('ur not authorised')
          }
        }
      })
      .catch(e => {
        setErr(true)
        setErrMsg('somthing wents wrong')
      })
  }

  return (
    <div className={css.main}>
      <Error err={err} errMsg={errMsg}  />

      <div className={css.login}>
        <div className={css.left}>
          <h1>Login</h1>
          <h5>
            Don't Have Account? <NavLink to='/reg'>Signup</NavLink>
          </h5>
          <h4>
            <NavLink to='/resetPass'>Forget Password?</NavLink>
          </h4>

          <form onSubmit={e => login(e)}>
            <Input
              type='email'
              name='email'
              labelName='email'
              value={email}
              onInput={e => setEmail(() => e.target.value)}
              id='email'
            />

            <Input
              type='password'
              name='password'
              labelName='password'
              len='8'
              value={pass}
              onInput={e => setPass(() => e.target.value)}
              id='password'
            />
            <Button type='submit' name='login' />
          </form>
        </div>
      </div>
    </div>
  )
}

export default Login
