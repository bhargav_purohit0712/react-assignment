import React, { useState } from 'react'

import css from './reset.module.css'

import Input from '../../ui/input/input'
import Error from '../../ui/error/error'
import Button from '../../ui/button/button'

import { NavLink } from 'react-router-dom'

function ResetP () {
  const [email, setEmail] = useState('')
  const [err, setErr] = useState(false)
  const [errMsg, setErrMsg] = useState('')

  const [sus, setSus] = useState(false)
  const [susMsg, setSusMsg] = useState('')

  const reset = p => {
    p.preventDefault()
    fetch('http://localhost:8080/api/resetPass', {
      method: 'POST',
      body: JSON.stringify({ email: email }),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(data => data.json())
      .then(res => {
        if (res.err) {
          setErr(true)
          setErrMsg(() => res.err)
        } else {
          setSus(true)
          setSusMsg(() => res.msg)
        }
      })
      .catch(err => {
        setErr(true)
        setErrMsg(() => err)
      })
  }
  return (
    <div className={css.main}>
      <Error err={err} errMsg={errMsg} sus={sus} susMsg={susMsg} />

      <div className={css.reset}>
        <div className={css.left}>
          <form onSubmit={e => reset(e)}>
            <h1>
              Don't Want to reset? <NavLink to='/login'>Login</NavLink>{' '}
            </h1>
            <Input
              type='email'
              name='email'
              labelName='email'
              value={email}
              onInput={e => setEmail(() => e.target.value)}
              id='email'
            />

            <Button type='submit' name='login' />
          </form>
        </div>
      </div>
    </div>
  )
}

export default ResetP
