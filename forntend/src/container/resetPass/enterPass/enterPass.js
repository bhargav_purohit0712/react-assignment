import React, { useState } from 'react'

import css from '../reset.module.css'
import Button from '../../../ui/button/button'
import Input from '../../../ui/input/input'

import Error from '../../../ui/error/error'
import { useHistory } from 'react-router-dom'

function EnterPass (props) {
  const [pass, setPass] = useState('')
  const [repass, setRepass] = useState('')
  const [err, setErr] = useState(false)
  const [errMsg, setErrMsg] = useState('')

  const history = useHistory()

  const reset = e => {
    e.preventDefault()
    if (pass === repass) {
      fetch('http://localhost:8080/api/resetTpass', {
        method: 'POST',
        body: JSON.stringify({ pass: pass, token: props.match.params.ID }),
        headers: {
          'Content-Type': 'application/json'
        }
      })
        .then(res => res.json())
        .then(data => {
          if (data.err) {
            setErr(true)
            setErrMsg(data.err)
          } else {
            history.replace('/login')
          }
        })
        .catch(err => {
          setErr(true)
          setErrMsg('somthing wents wrong try again later..')
        })
    } else {
      setErr(true)
      setErrMsg('both password are not same..')
    }
  }
  return (
    <div className={css.main}>
      <Error err={err} errMsg={errMsg} />

      <div className={css.reset}>
        <div className={css.left}>
          <form onSubmit={e => reset(e)}>
            <Input
              type='password'
              name='password'
              labelName='password'
              len='8'
              value={pass}
              onInput={e => setPass(() => e.target.value)}
              id='password'
            />
            <Input
              type='password'
              name='password'
              labelName='Confrim-password'
              len='5'
              value={repass}
              onInput={e => setRepass(() => e.target.value)}
              id='password2'
            />
            <Button type='submit' name='login' />{' '}
          </form>
        </div>
      </div>
    </div>
  )
}

export default EnterPass
