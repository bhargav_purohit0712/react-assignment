import React, { useState } from 'react'

import css from './reg.module.css'

import { useHistory } from 'react-router-dom'

import Input from '../../ui/input/input'
import Error from '../../ui/error/error'
import Button from '../../ui/button/button'

import { NavLink } from 'react-router-dom'

function Reg () {
  const [email, setEmail] = useState('')
  const [pass, setPass] = useState('')
  const [repass, setRepass] = useState('')
  const [name, setName] = useState('')
  const [err, setErr] = useState(false)
  const [errMsg, setErrMsg] = useState('')
  const history = useHistory()

  const reg = e => {
    e.preventDefault()
    if (pass === repass) {
      setErr(false)
      const data = {
        email: email,
        pass: pass,
        name: name
      }
      fetch('http://localhost:8080/api/user', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json'
        }
      })
        .then(res => res.json())
        .then(data => {
          if (data.msg) {
            history.replace('/login')
          } else {
            setErr(true)
            setErrMsg(data.err)
          }
        })
        .catch(e => alert('somthing wents wrong'))
    } else {
      setErr(true)
      setErrMsg(() => 'Pass Is Not Same..')
    }
  }

  return (
    <div className={css.main} id='main'>
      <Error err={err} errMsg={errMsg} />

      <div className={css.reg}>
        <div className={css.left}>
          <h1>SignUp</h1>
          <h5>
            Already Have Account? <NavLink to='/login'>SignIn</NavLink>
          </h5>
          <form onSubmit={e => reg(e)}>
            <Input
              name='name'
              id='name'
              type='text'
              len='3'
              labelName='name'
              value={name}
              onInput={e => setName(() => e.target.value)}
            />
            <Input
              name='email'
              id='email'
              type='email'
              labelName='email'
              value={email}
              onInput={e => setEmail(() => e.target.value)}
            />
            <Input
              name='pass'
              type='password'
              labelName='Password'
              len='8'
              id='pass'
              value={pass}
              onInput={e => setPass(() => e.target.value)}
            />
            <Input
              name='conpass'
              type='password'
              len='8'
              labelName='Confirm-Password'
              id='conpass'
              value={repass}
              onInput={e => setRepass(() => e.target.value)}
            />
            <Button type='submit' name='signUp' />
          </form>
        </div>
      </div>
    </div>
  )
}

export default Reg
