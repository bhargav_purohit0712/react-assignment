import React, { useState } from 'react'
import { Modal } from 'react-bootstrap'
import css from './addAcc.module.css'
import { IoMdAdd } from 'react-icons/io'

import { AiFillDelete } from 'react-icons/ai'
import Error from '../../../ui/error/error'

import Loading from '../../../ui/Loader/Loader'
import Input from '../../../ui/home/input/input'
import Button from '../../../ui/home/button/button'

import cookie from 'js-cookie'

const Addacc = props => {
  const [name, setName] = useState('')
  const [value, setValue] = useState(0)
  const [expance, setExpace] = useState([])
  const [err, setErr] = useState(false)
  const [errMsg, setErrMsg] = useState('')
  const [income, setIncome] = useState(0)
  const [accName, setAccName] = useState('')
  const [email, setEmail] = useState('')
  const [sus, setSus] = useState(false)
  const [susMsg, setSusMsg] = useState('')
  const [ulist, setUlist] = useState([])
  const [listOfEmail, setListOfEmail] = useState([])
  const [loading, setLoading] = useState(false)
  const addToServer = () => {
    if (accName === '') {
      setSus(false)

      setErrMsg('Please Enter Account Name')
      setErr(true)
    } else {
      setErr(false)
      setLoading(() => true)
      const du = ulist.length ? ulist : ''

      const data = {
        id: props.uid,
        name: accName,
        income: income,
        expance: expance,
        email: du
      }
      fetch('http://localhost:8080/api/account', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookie.get('token')
        }
      })
        .then(res => res.json())
        .then(data => {
          if (data.err) {
            if (data.err.status) {
              cookie.remove('uid')
              cookie.remove('token')
              props.del()
            } else {
              setLoading(() => false)
              setErrMsg(() => data.err)
              setErr(() => true)
            }
          } else {
            setLoading(() => false)

            setErr(() => false)
            setSusMsg(() => data.msg)
            setSus(() => true)

            setIncome(0)
            setName('')
            setAccName('')
            setEmail('')
            setValue(0)
            setExpace([])
            setListOfEmail([])
            setUlist([])
            setErr(false)
            setSus(false)
            props.del()

            props.onHide()
          }
        })
        .catch(err => {
          setSus(false)
          setLoading(() => false)
          props.del()

          setErrMsg('somthing want wrong...')
          setErr(true)
        })
    }
  }

  const addExpace = () => {
    setExpace(old => [
      ...old,
      { key: Date.now().toString(), name: name, value: value }
    ])
    setName('')
    setValue(0)
  }
  const delExpance = key => {
    const newExp = expance.filter(exp => {
      return exp.key !== key
    })
    setExpace(() => newExp)
  }
  const addEmail = () => {
    const data = { email: email, uid: props.uid }
    fetch('http://localhost:8080/api/checkEmail', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookie.get('token')
      }
    })
      .then(res => res.json())
      .then(data => {
        if (data.err) {
          if (data.err.status) {
            cookie.remove('uid')
            cookie.remove('token')
            props.del()
          } else {
            setErrMsg(() => data.err)
            setErr(() => true)
          }
        } else {
          setErr(() => false)
          setUlist(oul => [...oul, data.uid])
          setListOfEmail(ole => [...ole, { key: data.uid, email: email }])
          setEmail('')
        }
      })
      .catch(err => {
        setSus(false)
        setErrMsg('somthing want wrong...')
        setErr(true)
      })
  }
  const delEmail = key => {
    const delEmailele = listOfEmail.filter(email => {
      return email.key !== key
    })
    const delListOfUser = ulist.filter(ul => ul !== key)
    setUlist([...delListOfUser])
    setListOfEmail([...delEmailele])
  }
  const listOfExpance = expance.length
    ? expance.map(sv => (
        <h3 key={sv.key}>
          {' '}
          {sv.name}-{sv.value} ₹
          <AiFillDelete color='red' onClick={() => delExpance(sv.key)} />
        </h3>
      ))
    : null
  const showlistOfEmail = listOfEmail.length
    ? listOfEmail.map(le => (
        <h2 key={le.key}>
          {le.email}
          <AiFillDelete color='red' onClick={() => delEmail(le.key)} />
        </h2>
      ))
    : null
  return loading ? (
    <Loading />
  ) : (
    <Modal
      show={props.show}
      size='xl'
      aria-labelledby='contained-modal-title-vcenter'
      backdrop='static'
      centered
    >
      <Modal.Header>
        <Modal.Title>
          Add Your Expance{' '}
          <Error err={err} errMsg={errMsg} sus={sus} susMsg={susMsg} />{' '}
        </Modal.Title>
      </Modal.Header>

      <Modal.Body className={css.mbody}>
        <h4>
          <Input
            type='text'
            placeholder='Enter Account Name'
            valued={accName}
            method={e => setAccName(e.target.value)}
          />
          <Input
            type='number'
            valued={income}
            method={e => setIncome(e.target.value)}
            placeholder='Enter Income'
          />
          ₹
        </h4>
        <h1>Enter Your Expance</h1>
        {listOfExpance}
        <h4>
          <Input
            type='text'
            method={e => setName(() => e.target.value)}
            valued={name}
            placeholder='Enter Expance Name'
          />{' '}
          <Input
            valued={value}
            type='number'
            method={e => setValue(() => e.target.value)}
            placeholder='Enter Expance Amount'
          />
          <IoMdAdd className={css.addBtn} onClick={() => addExpace()} />
        </h4>
        <h3>
          <Input
            type='email'
            valued={email}
            method={e => setEmail(() => e.target.value)}
            placeholder='Enter Shared Account'
          />
          <IoMdAdd className={css.addBtn} onClick={() => addEmail()} />
        </h3>
        {showlistOfEmail}
      </Modal.Body>
      <Modal.Footer className={css.mfooter}>
        <Button method={() => props.onHide()} text='close' />

        <Button method={() => addToServer()} text='Submit' />
      </Modal.Footer>
    </Modal>
  )
}

export default Addacc
