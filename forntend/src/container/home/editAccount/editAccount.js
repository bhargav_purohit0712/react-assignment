import React, { useEffect, useState } from 'react'
import css from './editAccount.module.css'
import { Modal } from 'react-bootstrap'
import cookie from 'js-cookie'

import { IoMdAdd } from 'react-icons/io'
import Error from '../../../ui/error/error'
import { AiFillDelete, AiFillEdit } from 'react-icons/ai'

import Loading from '../../../ui/Loader/Loader'
import Input from '../../../ui/home/input/input'
import Button from '../../../ui/home/button/button'

const Editccount = props => {
  const [name, setName] = useState('')
  const [value, setValue] = useState(0)
  const [expance, setExpace] = useState([])
  const [err, setErr] = useState(false)
  const [errMsg, setErrMsg] = useState('')
  const [income, setIncome] = useState(0)
  const [accName, setAccName] = useState('')
  const [email, setEmail] = useState('')
  const [sus, setSus] = useState(false)
  const [susMsg, setSusMsg] = useState('')
  const [ulist, setUlist] = useState([])
  const [listOfEmail, setListOfEmail] = useState([])
  const [loading, setLoading] = useState(true)

  const addToServer = () => {
    if (accName === '') {
      setSus(false)

      setErrMsg('Please Enter Account Name')
      setErr(true)
    } else {
      setLoading(() => true)
      setErr(false)
      const du = ulist.length ? ulist : ''

      const data = {
        aid: props.aid,
        name: accName,
        income: income,
        expance: expance,
        email: du
      }
      fetch('http://localhost:8080/api/account', {
        method: 'PUT',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + cookie.get('token')
        }
      })
        .then(res => res.json())
        .then(data => {
          if (data.err) {
            if (data.err.status) {
              cookie.remove('uid')
              cookie.remove('token')
              props.del()
            } else {
              setLoading(() => false)
              setErrMsg(() => data.err)
              setErr(() => true)
            }
          } else {
            setErr(() => false)
            setSusMsg(() => data.msg)
            setSus(() => true)
            setLoading(() => false)

            setIncome(0)
            setName('')
            setAccName('')
            setEmail('')
            setValue(0)
            setExpace([])
            setListOfEmail([])
            setUlist([])
            setErr(false)
            setSus(false)

            props.onHide()
            props.del()
          }
        })
        .catch(err => {
          setSus(false)
          setLoading(() => false)
          props.del()

          setErrMsg('somthing want wrong...')
          setErr(true)
        })
    }
  }

  useEffect(() => {
    fetch('http://localhost:8080/api/oneAccount/' + props.aid, {
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + cookie.get('token')
      }
    })
      .then(res => res.json())
      .then(resdata => {
        if (resdata.err) {
          cookie.remove('token')
          props.del()
        } else {
          setAccName(() => resdata.data.name)
          setIncome(() => resdata.data.income)
          setExpace(() => resdata.data.expance)
          setListOfEmail(() => resdata.data.users)
          const getid = resdata.data.users.map(u => u._id)
          setUlist(() => getid)
          setLoading(() => false)
        }
      })
      .catch(err => console.log('hi', err))
  }, [])

  const delEmail = key => {
    const delEmailele = listOfEmail.filter(email => {
      return email._id !== key
    })
    const delListOfUser = ulist.filter(ul => ul !== key)
    setUlist(() => [...delListOfUser])
    setListOfEmail(() => [...delEmailele])
  }
  const editExpace = key => {
    const setField = expance.filter(exp => exp.key === key)
    setName(() => setField[0].name)
    setValue(() => setField[0].value)
    const newExp = expance.filter(exp => {
      return exp.key !== key
    })
    setExpace(() => newExp)
  }
  const delExpance = key => {
    const newExp = expance.filter(exp => {
      return exp.key !== key
    })
    setExpace(() => newExp)
  }

  const listOfExpance = expance.length
    ? expance.map(sv => (
        <h3 key={sv.key} className={css.data}>
          <AiFillEdit color='indigo' onClick={() => editExpace(sv.key)} />
          <AiFillDelete color='red' onClick={() => delExpance(sv.key)} />{' '}
          {sv.name}-{sv.value} ₹
        </h3>
      ))
    : null

  const showlistOfEmail = listOfEmail.length
    ? listOfEmail.map(le => (
        <h2 key={le._id} className={css.data}>
          {le.email}
          <AiFillDelete color='red' onClick={() => delEmail(le._id)} />
        </h2>
      ))
    : null

  const addExpace = () => {
    setExpace(old => [
      ...old,
      { key: Date.now().toString(), name: name, value: value }
    ])
    setName('')
    setValue(0)
  }

  const addEmail = () => {
    const data = { email: email, uid: props.uid }
    fetch('http://localhost:8080/api/checkEmail', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookie.get('token')
      }
    })
      .then(res => res.json())
      .then(data => {
        if (data.err) {
          if (data.err.status) {
            cookie.remove('token')

            cookie.remove('uid')
            props.del()
          } else {
            setErrMsg(() => data.err)
            setErr(() => true)
          }
        } else {
          setErr(() => false)
          setUlist(oul => [...oul, data.uid])
          setListOfEmail(ole => [...ole, { _id: data.uid, email: email }])
          setEmail('')
        }
      })
      .catch(err => {
        setSus(false)
        setErrMsg('somthing want wrong...')
        setErr(true)
      })
  }

  return loading ? (
    <Loading />
  ) : (
    <Modal
      show={props.show}
      size='xl'
      aria-labelledby='contained-modal-title-vcenter'
      backdrop='static'
      centered
    >
      <Modal.Header>
        <Modal.Title>
          <Error err={err} errMsg={errMsg} sus={sus} susMsg={susMsg} />{' '}
          <div className={css.data + ' ' + css.title}>
            <Input
              type='text'
              valued={accName}
              method={e => setAccName(() => e.target.value)}
              placeholder='edit account name'
            />
          </div>{' '}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className={css.mbody}>
        <div className={css.income}>
          <h3>Your Income </h3>

          <div className={css.data}>
            {' '}
            <Input
              valued={income}
              method={e => setIncome(e.target.value)}
              type='number'
              placeholder='Enter Expance Amount'
            />{' '}
            ₹
          </div>
        </div>
        <hr />
        <div>
          <h3>Your Expance</h3>
          {listOfExpance}
          <Input
            type='text'
            method={e => setName(() => e.target.value)}
            valued={name}
            placeholder='Enter Expance Name'
          />{' '}
          <Input
            method={e => setValue(() => e.target.value)}
            valued={value}
            type='number'
            placeholder='Enter Expance Amount'
          />
          <IoMdAdd className={css.addBtn} onClick={() => addExpace()} />
        </div>
        <hr />
        <h2>Shared Accounts</h2>
        {showlistOfEmail}
        <Input
          type='email'
          valued={email}
          method={e => setEmail(() => e.target.value)}
          placeholder='Enter Shared Account'
        />
        <IoMdAdd className={css.addBtn} onClick={() => addEmail()} />
      </Modal.Body>
      <Modal.Footer className={css.mfooter}>
        <Button method={() => props.onHide()} text='close' />
        <Button method={() => addToServer()} text='Update' />
      </Modal.Footer>
    </Modal>
  )
}

export default Editccount
