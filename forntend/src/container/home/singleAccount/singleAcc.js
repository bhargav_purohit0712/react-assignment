import React, { useState, useEffect } from 'react'

import css from './single.module.css'
import { Modal } from 'react-bootstrap'
import Loading from '../../../ui/Loader/Loader'
import cookie from 'js-cookie'

const SingleAcc = props => {
  const [data, setData] = useState({})
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    fetch('http://localhost:8080/api/oneAccount/' + props.aid, {
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + cookie.get('token')
      }
    })
      .then(res => res.json())
      .then(data => {
        if (data.err) {
          if (data.err.status) {
            cookie.remove('token')

            cookie.remove('uid')
            props.del()
          }
        } else {
          setData(() => data.data)
          setLoading(() => false)
        }
      })
      .catch(err => {})
  }, [])

  const listOfAcc = data
    ? data.users
      ? data.expance.map(su => (
          <div className={css.data} key={su.key}>
            {' '}
            {su.name} - {su.value}{' '}
          </div>
        ))
      : null
    : null
  let total = 0
  const totalExpace = data
    ? data.users
      ? data.expance.map(su => (total += +su.value))
      : null
    : null
  const usersList = data
    ? data.users
      ? data.users.map(su => (
          <div className={css.data} key={su._id}>
            {' '}
            {su.name} - {su.email}{' '}
          </div>
        ))
      : null
    : null
  let remaningMoney = data ? +data.income - +total : null
  return loading ? (
    <Loading />
  ) : (
    <Modal
      show={props.show}
      onHide={props.onHide}
      size='xl'
      aria-labelledby='contained-modal-title-vcenter'
      backdrop='static'
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title>
          <div className={css.data + ' ' + css.title}>
            {data ? data.name : null}
          </div>{' '}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className={css.income}>
          <h3>Your Income </h3>
          <div className={css.data}>{data ? data.income : null} ₹</div>
        </div>
        <hr />
        <div>
          <h3>Your Expance</h3>
          {listOfAcc}
        </div>
        <hr />
        <h2>Shared Accounts</h2>
        {usersList}
      </Modal.Body>
      <Modal.Footer className={css.mfooter}>
        <div>
          <h3>Total Expace - {total} ₹</h3>
          <h3>Remaning Money - {remaningMoney} ₹</h3>
        </div>
      </Modal.Footer>
    </Modal>
  )
}

export default SingleAcc
