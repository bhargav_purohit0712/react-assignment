import React, { useState } from 'react'
import cookie from 'js-cookie'
import { MdAccountBalanceWallet } from 'react-icons/md'
import css from './accList.module.css'
import { Card } from 'react-bootstrap'
import { BiShow } from 'react-icons/bi'
import Error from '../../../ui/error/error'

import { AiFillEdit, AiFillDelete } from 'react-icons/ai'
import Loading from '../../../ui/Loader/Loader'

const AccountList = props => {
  const [err, setErr] = useState(false)
  const [errMsg, setErrMsg] = useState('')
  const [loading, setLoading] = useState(false)

  const delAcc = (id, aid) => {
    setLoading(true)
    const data = {
      aid: aid,
      id: id
    }
    fetch('http://localhost:8080/api/account', {
      method: 'DELETE',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + cookie.get('token')
      }
    })
      .then(res => res.json())
      .then(data => {
        if (data.err) {
          if (data.err.status) {
            cookie.remove('uid')
            cookie.remove('token')
            props.del()
          }
          setErrMsg(() => data.err)
          setErr(() => true)
          setLoading(false)
        } else {
          props.del()
          setLoading(false)
        }
      })
      .catch(err => console.log(err))
  }

  return loading ? (
    <Loading />
  ) : (
    <div className={css.accList} key={props.uid}>
      <Card>
        <Card.Header className={css.name}>
          <Error err={err} errMsg={errMsg} />{' '}
          <MdAccountBalanceWallet className={css.addBtn} /> {props.name}
        </Card.Header>
        <Card.Body>
          <Card.Text className={css.income}>
            Total Income - {props.income}
            <br />
            <AiFillEdit
              className={css.addBtn2}
              onClick={() => props.showEdiitAcc(props.aid)}
            />
            <BiShow
              className={css.addBtn2}
              onClick={() => props.showSingleAcc(props.aid)}
            />
            <AiFillDelete
              className={css.delBtn}
              onClick={() => delAcc(props.uid, props.aid)}
            />
          </Card.Text>
        </Card.Body>
      </Card>
    </div>
  )
}

export default AccountList
