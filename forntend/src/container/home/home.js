import React, { useEffect, useRef, useState } from 'react'
import { useHistory } from 'react-router-dom'
import cookie from 'js-cookie'
import css from './home.module.css'

import AccList from './accountsList/accouny_List'
import SingleAcc from './singleAccount/singleAcc'
import EditAccount from './editAccount/editAccount'
import AddAcc from './addAccount/addAccount'

import Loading from '../../ui/Loader/Loader'


import { IoMdAdd } from 'react-icons/io'
import { AiOutlineLogout } from 'react-icons/ai'

function Home () {
  const uid = useRef()
  const [data, setData] = useState({})
  const [del, setDel] = useState(true)

  const history = useHistory()
  const [signleAccModel, setSingleAccModel] = useState(false)
  const [editModel, setEditModel] = useState(false)
  const [modalShow, setModalShow] = useState(false)
  const aid = useRef()
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    if (cookie.get('uid') && cookie.get('token')) {
      uid.current = cookie.get('uid')

      fetch('http://localhost:8080/api/account/' + cookie.get('uid'), {
        method: 'GET',
        headers: {
          Authorization: 'Bearer ' + cookie.get('token')
        }
      })
        .then(res => res.json())
        .then(data => {
          if (data.err) {
            cookie.remove('uid')
            cookie.remove('token')
            history.replace('/login')
          } else {
            setLoading(() => false)

            setData(() => data.data)
          }
        })
        .catch(err => console.log(err))
    } else {
      history.replace('/login')
    }
  }, [del])

  const showSingleAccount = said => {
    aid.current = said
    setSingleAccModel(() => true)
  }
  const showEditAccount = said => {
    aid.current = said
    setEditModel(true)
  }

  const accList = !data.account ? null : data.account.length ? (
    data.account.map(s => (
      <AccList
        showSingleAcc={aid => showSingleAccount(aid)}
        showEdiitAcc={aid => showEditAccount(aid)}
        aid={s._id}
        name={s.name}
        uid={uid.current}
        income={s.income}
        key={s._id}
        del={() => setDel(old => !old)}
      />
    ))
  ) : (
    <h3>enter some expance </h3>
  )
  const logOut = () => {
    cookie.remove('uid')
    cookie.remove('token')
    cookie.remove('aid')
    history.replace('/login')
  }
  return loading ? (
    <Loading />
  ) : (
    <React.Fragment>
      <div className={css.main}>
        <div className={css.addExpance}>
          <IoMdAdd className={css.addBtn} onClick={() => setModalShow(true)} />
          {modalShow ? (
            <AddAcc
              uid={uid.current}
              del={() => setDel(old => !old)}
              show={modalShow}
              onHide={() => setModalShow(false)}
            />
          ) : null}

          {signleAccModel ? (
            <SingleAcc
              del={() => setDel(old => !old)}
              aid={aid.current}
              show={signleAccModel}
              onHide={() => setSingleAccModel(false)}
              uid={uid.current}
            />
          ) : null}

          {editModel ? (
            <EditAccount
              aid={aid.current}
              uid={uid.current}
              del={() => setDel(old => !old)}
              show={editModel}
              onHide={() => setEditModel(false)}
            />
          ) : null}
        </div>
        <div className={css.listExpace}>{accList}</div>
      </div>
      <footer>
        <div className={css.footer}>
          <AiOutlineLogout className={css.addBtn} onClick={() => logOut()} />
        </div>
      </footer>
    </React.Fragment>
  )
}

export default Home
