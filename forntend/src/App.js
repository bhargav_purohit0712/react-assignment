import './App.css'

import { useState, useEffect } from 'react'
import { Route, Switch } from 'react-router-dom'

import Login from './container/login/login'
import Home from './container/home/home'
import Reg from './container/registration/reg'
import RePass from './container/resetPass/resetP'
import Enterp from './container/resetPass/enterPass/enterPass'

import cookie from 'js-cookie'

function App () {
  const [login, setLogin] = useState(false)
  useEffect(() => {
    if (cookie.get('uid') && cookie.get('token')) {
      setLogin(() => true)
    } else {
      setLogin(() => false)
    }
  }, [login, cookie.get('uid'), cookie.get('token')])
  return (
    <div className='App'>
      <Switch>
        <Route path='/login' component={Login} />
        <Route exact path='/home' component={Home} />
        <Route path='/reg' component={Reg} />
        <Route path='/repass/:ID' component={Enterp} />
        <Route path='/resetPass' component={RePass} />
        <Route path='/' component={login ? Home : Login} />
      </Switch>
    </div>
  )
}

export default App
