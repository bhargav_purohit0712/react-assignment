const crypto = require('crypto')

const mongoose = require('mongoose')

const { validationResult } = require('express-validator')

const User = require('../model/user')
const Account = require('../model/account')

const jwt = require('jsonwebtoken')

const nodemailer = require('nodemailer')
const sendgridtransport = require('nodemailer-sendgrid-transport')

const transporter = nodemailer.createTransport(
  sendgridtransport({
    auth: {
      api_key:
        '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'
    }
  })
)
// for pass
const bcrypt = require('bcryptjs')

exports.createUser = (req, res, next) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    const error = new Error('Validation failed.')
    error.statusCode = 422
    error.data = errors.array()
    res.status(422).json({ err: error })
  } else {
    const name = req.body.name
    const pass = req.body.pass
    const email = req.body.email
    User.findOne({ email: email }).then(userDoc => {
      if (userDoc) {
        return res.status(355).json({ err: 'E-Mail address already exists!' })
      } else {
        bcrypt.hash(pass, 12).then(hashedPw => {
          const user = new User({
            email: email,
            pass: hashedPw,
            name: name,
            account: []
          })
          const acc = Account({
            users: user,
            owner: user,
            name: 'Default',
            income: 0,
            expance: [
              {
                name: 'temp',
                price: 0
              }
            ]
          })
          acc.save().then(rest => {
            user.account = acc
            return user
              .save()
              .then(result => {
                res
                  .status(201)
                  .json({ msg: 'User created!', userId: result._id })

                return transporter
                  .sendMail({
                    to: email,
                    from: 'bhargav.purohit0712@gmail.com',
                    subject: 'SignUp sucessfully!!',
                    html: '<h1> You successfully signed up!</h1>'
                  })
                  .catch(err => console.log(err))
              })
              .catch(err => {
                res.json(401).json({
                  err: err
                })
              })
          })
        })
      }
    })
  }
}

exports.login = async (req, res, next) => {
  const email = req.body.email
  const password = req.body.pass
  let loadedUser
  try {
    const user = await User.findOne({ email: email })
    if (!user) {
      res.status(401).json({ err: 'A user with this email could not be found' })
    } else {
      loadedUser = user
      isEqual = await bcrypt.compare(password, user.pass)

      if (!isEqual) {
        res.status(401).json({ err: 'Wrong password!' })
      } else {
        const token = await jwt.sign(
          {
            email: loadedUser.email,
            userId: loadedUser._id.toString()
          },
          'somesupersecretsecret',
          { expiresIn: '1h' }
        )

        res
          .status(200)
          .json({ token: token, userId: loadedUser._id.toString() })
      }
    }
  } catch (error) {
    res.json({ err: error })
  }
}
exports.getOneAccount = async (req, res, next) => {
  try {
    const id = req.params.id
    const account = await Account.findById(id).populate('users', 'email name')
    res.status(200).json({ data: account })
  } catch (error) {
    res.status(400).json({ err: 'somthing wrong..' })
  }
}

exports.getAcc = (req, res, next) => {
  const id = req.params.id
  User.findById(id)
    .select('account')
    .populate('account', 'name income createdAt', null, {
      sort: { createdAt: -1 }
    })
    .then(data => {
      res.status(200).json({ data: data })
    })
    .catch(err => {
      console.log(err)
      res.status(400).json({ err: err })
    })
}

exports.addAccToUser = async (req, res, next) => {
  const id = req.body.id
  const name = req.body.name
  const income = req.body.income
  const expance = req.body.expance
  const email = req.body.email
  try {
    const user = await User.findById(id)

    if (email === '') {
      const acc = Account({
        name: name,
        owner: user,
        users: user,
        expance: [...expance],
        income: income
      })
      user.account.push(acc)
      await acc.save()
      await user.save()
      res.status(200).json({ msg: 'created', data: { id: acc._id } })
    } else {
      const emailToUid = email.map(e => mongoose.Types.ObjectId(e))

      const acc = Account({
        name: name,
        owner: user,
        users: [...emailToUid],
        expance: [...expance],
        income: income
      })
      user.account.push(acc)
      await acc.save()
      await user.save()
      await email.map(e => {
        User.findById(e)
          .then(res => {
            res.account.push(acc)
            res.save()
          })
          .catch(err => {
            res.status(400).json({ err: err })
          })
      })
      res.status(200).json({ msg: 'created', data: { id: acc._id } })
    }
  } catch (error) {
    console.log(error)
    res.status(401).json({ err: 'somthing wrong' })
  }
}

exports.updateAcc = async (req, res, next) => {
  const aid = req.body.aid
  const name = req.body.name
  const income = req.body.income
  const expance = req.body.expance
  const email = req.body.email
  try {
    if (email === '') {
      const acc = await Account.findById(aid)
      //const emailToUid = email.map(e => mongoose.Types.ObjectId(e))
      acc.users.map(e =>
        User.findById(e)
          .then(u => {
            const updateAcc = u.account.filter(acco => acco != aid)
            u.account = [...updateAcc]
            u.save()
          })
          .then(r => {
            acc.name = name
            acc.income = income
            acc.expance = [...expance]
            acc.users = []
            acc.save()
          })
          .then(r => {
            res.status(200).json({ msg: 'updated' })
          })
          .catch(err => console.log(err))
      )
    } else {
      const emailToUid = email.map(e => mongoose.Types.ObjectId(e))

      const acc = await Account.findById(aid)

      acc.name = name
      acc.income = income
      acc.expance = [...expance]
      acc.users = [...emailToUid]

      await acc.save()
      await email.map(e => {
        User.findById(e)
          .then(res => {
            res.account.push(acc)
            res.save()
          })
          .catch(err => {
            res.status(400).json({ err: err })
          })
      })

      res.status(200).json({ msg: 'updated' })
    }
  } catch (error) {
    console.log(error)
    res.status(401).json({ err: 'somthing wrong' })
  }
}

exports.checkEmail = async (req, res, next) => {
  const email = req.body.email
  const uid = req.body.uid
  try {
    const uidUser = await User.findById(uid)
    if (uidUser.email === email) {
      res.status(400).json({ err: 'Your Owner Of This Account' })
    } else {
      const user = await User.findOne({ email: email })
      if (user !== null) {
        res.status(200).json({ uid: user._id })
      } else {
        res.status(401).json({ err: 'User Not Found' })
      }
    }
  } catch (error) {
    res.status(400).json({ err: 'somthing wents wrong..' })
  }
}

exports.deleteAccount = async (req, res, next) => {
  const id = req.body.id
  const aid = req.body.aid

  try {
    const acc = await Account.findById(aid)

    if (acc.owner == id) {
      await User.findById(id)
        .then(u => {
          const updateAcc = u.account.filter(acco => acco != aid)
          u.account = [...updateAcc]
          u.save()
        })
        .catch(e => {
          console.log(e)
        })

      await acc.users.map(e =>
        User.findById(e)
          .then(u => {
            const updateAcc = u.account.filter(acco => acco != aid)
            u.account = [...updateAcc]
            u.save()
          })
          .catch(e => {
            console.log(e)
          })
      )

      await Account.findByIdAndDelete(aid)
      res.status(200).json({ msg: 'deleted' })
    } else {
      res.status(401).json({ err: 'you are not owner of this account' })
    }
  } catch (error) {
    console.log(error)
    res.status(401).json({ err: 'somthing wrong' })
  }
}
exports.resetPass = (req, res, next) => {
  const email = req.body.email

  crypto.randomBytes(32, (err, buffer) => {
    const token = buffer.toString('hex')
    User.findOne({ email: email }).then(user => {
      if (!user) {
        res.status(401).json({ err: 'email not exist' })
      } else {
        user.resetToken = token
        user.resetTokenExpire = Date.now() + 3600000
        return user
          .save()
          .then(rest => {
            transporter
              .sendMail({
                to: email,
                from: 'bhargav.purohit0712@gmail.com',
                subject: 'SignUp sucessfully!!',
                html: `<p> You requested a reset password </p>
              </p> Click This <a href="http://localhost:3000/repass/${token}">here </a> <p>
              `
              })
              .then(sent => res.json({ msg: 'check your mail' }))
              .catch(err => console.log(err))
          })
          .catch(err => console.log(err))
      }
    })
  })
}

exports.resetWithToken = async (req, res, next) => {
  const token = req.body.token
  const pass = req.body.pass
  try {
    const user = await User.findOne({
      resetToken: token,
      resetTokenExpire: { $gt: Date.now() }
    })
    if (!user) {
      res.status(400).json({ err: 'your token is expire' })
    } else {
      const hp = await bcrypt.hash(pass, 12)
      user.pass = hp
      user.save()
      res.status(300).json({ msg: 'sus..' })
    }
  } catch (error) {
    res.status(400).json({ err: 'somthing wents wrong' })
  }
}
