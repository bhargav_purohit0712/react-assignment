const mongoose = require('mongoose')

const Schema = mongoose.Schema

const userSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    income: { type: Number, require: true },
    expance: [Object],
    users: [
      {
        type: Schema.Types.ObjectId,
        ref: 'User'
      }
    ],
    owner: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    }
  },
  { timestamps: true }
)

module.exports = mongoose.model('Account', userSchema)
