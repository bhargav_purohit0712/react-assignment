const mongoose = require('mongoose')

const Schema = mongoose.Schema

const userSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  resetToken: String,
  resetTokenExpire: Date,
  pass: {
    type: String,
    required: true
  },
  account: {
    type: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Account'
      }
    ]
  }
})

module.exports = mongoose.model('User', userSchema)
