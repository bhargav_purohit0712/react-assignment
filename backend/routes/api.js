const express = require('express')
const route = express.Router()

const auth = require('../middleware/auth')

const { body } = require('express-validator')

const apiController = require('../controllers/api')

//create user
route.post(
  '/user',
  [
    body('email')
      .isEmail()
      .withMessage('Please enter a valid email.')
      .normalizeEmail(),
    body('pass')
      .trim()
      .isLength({ min: 8 }),
    body('name')
      .trim()
      .not()
      .isEmpty()
  ],

  apiController.createUser
)

//login user
route.post(
  '/login',
  [
    body('email')
      .isEmail()
      .withMessage('Please enter a valid email.')
      .normalizeEmail(),
    body('pass')
      .trim()
      .isLength({ min: 8 })
  ],
  apiController.login
)

route.post('/checkEmail', auth, apiController.checkEmail)

//reset pass
route.post('/resetPass', apiController.resetPass)

//reset set with token
route.post('/resetTpass', apiController.resetWithToken)

//get single acc
route.get('/oneAccount/:id', auth, apiController.getOneAccount)

//get user account
route.get('/account/:id', auth, apiController.getAcc)

//add account to user
route.post('/account', auth, apiController.addAccToUser)

//add data to acc
route.put('/account', auth, apiController.updateAcc)

//delete acc of user
route.delete('/account', auth, apiController.deleteAccount)

module.exports = route
