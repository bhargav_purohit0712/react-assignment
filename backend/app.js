const express = require('express')
const mongoose = require('mongoose')

//for url data
const bodyParser = require('body-parser')

const app = express()

//for routs
const api = require('./routes/api')

//for simple data
//app.use(bodyParser.urlencoded({ extended: false }))

//for json parsing
app.use(bodyParser.json()) // application/json

//for CORS

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader(
    'Access-Control-Allow-Methods',
    'OPTIONS, GET, POST, PUT, PATCH, DELETE'
  )
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Content-Type, Authorization,  X-Requested-With'
  )
  next()
})

//for restapi data
app.use('/api', api)

mongoose
  .connect(
    'mongodb+srv://bhargav:Bunny12345@cluster0.xyg3r.mongodb.net/expance?retryWrites=true&w=majority',
    { useUnifiedTopology: true, useNewUrlParser: true }
  )
  .then(res => {
    console.log('connected')
    app.listen(8080)
  })
  .catch(err => console.log('hi'))
