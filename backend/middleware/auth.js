const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
  const token = req.get('Authorization').split(' ')[1]
  let decodeToken
  try {
    decodeToken = jwt.verify(token, 'somesupersecretsecret')
  } catch (e) {
    return res
      .status(401)
      .json({ err: { msg: 'ur not authorised', status: '401' } })
  }
  if (!decodeToken) {
    return res
      .status(401)
      .json({ err: { msg: 'ur not authorised', status: '401' } })
  }
  req.uId = decodeToken.userId
  next()
}
